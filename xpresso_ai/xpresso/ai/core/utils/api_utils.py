from xpresso.ai.core.commons.network.http.send_request import SendHTTPRequest
from xpresso.ai.core.commons.network.http.http_request import HTTPMethod
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.commons.utils.constants import CONTROLLER_FIELD, \
    SERVER_URL_FIELD
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    ControllerClientResponseException, ControllerAPICallException


class APIUtils:
    """
    utils class with useful api calls to xpresso controller
    """
    def __init__(self):
        """
        constructor method with default attributes setup
        """
        self.config = XprConfigParser()
        server_url = self.config[CONTROLLER_FIELD][SERVER_URL_FIELD]
        self.server_endpoint_run = f"{server_url}/run/api"
        self.server_endpoint_experiment = f"{server_url}/exp/api"
        self.server_endpoint_project = f"{server_url}/project/api"
        self.request_module = SendHTTPRequest()
        self.logger = XprLogger()

    def get_run_info(self, run_filter: dict):
        """"""
        try:
            response = self.request_module.send(
                self.server_endpoint_run, HTTPMethod.GET, run_filter
            )
            return response
        except ControllerClientResponseException as err:
            raise ControllerAPICallException(err.message, err.error_code)

    def update_run_info(self, update_info: dict):
        """"""
        try:
            self.request_module.send(
                self.server_endpoint_run, HTTPMethod.PUT, update_info
            )
        except ControllerClientResponseException as err:
            raise ControllerAPICallException(err.message, err.error_code)

    def get_experiment_info(self, experiment_filter: dict):
        """"""
        try:
            response_expt = self.request_module.send(
                self.server_endpoint_experiment, HTTPMethod.GET,
                experiment_filter
            )
            return response_expt
        except ControllerClientResponseException as err:
            raise ControllerAPICallException(err.message, err.error_code)

    def get_project_info(self, project_filter: dict):
        """"""
        try:
            response_project = self.request_module.send(
                self.server_endpoint_project, HTTPMethod.GET, project_filter
            )
            return response_project
        except ControllerClientResponseException as err:
            raise ControllerAPICallException(err.message, err.error_code)
